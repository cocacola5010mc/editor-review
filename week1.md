# Week One

Welcome to my thoughts on editing code and how I would like to improve it to increse productivity and enjoyment.

## Recent Struggles with editing

I have been using vim for the good part of a year while using a colemak keyboard layout. For that setup I had configured hjkl mapped to neio and for insert mode ctrl + neio for movement which allowed for smooth movement without the need for leaving insert mode as often. About a month ago I stopped using the colemak keyboard layout in favor of the qwerty layout. I went to go setup the ctrl + hjkl but disaster struck! ctrl + h is the same key sequence as backspace in a terminal which originates back to way back when there was no backspace on keyboards. The backspace keybind is then unbindable so I'm unable to use those insert mode navigation shortcuts.

### Modal Editing

With that I was starting to become irritated by modal editing and having to switch into normal mode to move the cursor to then switch back into insert mode for typing and was starting to revert to using the arrow keys which was rather anoying as its moving away from the home row and which was also irritating me as it was also just an "incorrect" method of moving the cursor.

## My Current Setup

### On Emacs

I have been through the in built emacs tutorial and learned the rather sane navigation keybinds aswell as the basic utitity of opening files and directorys and dealing with windows, buffers, and terminals.

I do quite like emacs but I still dont have a stable configuration that provides the more IDE like environment that I would like to transition back to from my more minimalist vim configuration so I have made the choice to go with vscode untill I can get it how I would like it.

### Then VSCode

So, it seems like a bit of an insane choice to make going from vim to vscode when you want to use emacs but for me, it was the most sensible thing to do as I was starting to really find editing in vim rather unpleasant and became less productive so I decided to make the change to using vscode with the plugin "awesome emacs" which provides emacs bindings for vscode. For other plugins, i have also installed ESlint, Gruvbox Material, and Prettier which provides some Javasript and Typescript linting, auto formatting, and themeing and are all the vscode variant of some of the plugins that I used in vim.

I am currently really enjoying using vscode as it is much more freeing than using a mode based editor like vim. There is also the intellisence auto completion that comes with vscode which is just brilliant, its amazingly snappy and comes up with amazingly accurate options. In vim I did not use any auto completion so this is currently very much of a novelty to me. I have mainly been writing typescript which seems to be the intended language for vscode, but I'm excited to continue working on some of my haskell projects to see how that gets handled.

### What I would like to learn

- The main thing that I would like to learn continuing on is emacs selections with line duplications, deletions, and removals
- I would also like to learn how to jump between files in a project without using the side tree similar to how fzf works inside vim.
- Do something with terminals. I'm not a huge fan of the piddly terminal at the bottom and as terminals are where I do everything like moving, copying, managing, and transforming files and most other actions that would normally be performed in a gui. In an ideal world, I would quite like to have a text editing buffer be able to be opened up as a terminal via a keybind with a modifier key to choose between whether its in the project dir or just my home. I'm looking to see if there is a plugin for this, or whether its something that is fesable to implement on my own.

  ***

### Note

I dont plan on focusing too much on paragraph structure or spelling as this is more for the study of my editing experience but if anyone is reading there ramblings and wants to suggest an improvement, feel free to provide a merge/pull request.
